# ltbfs - a filesystem created by Logan Bateman

## Build instructions
You must have `cmake`, `make`, `libfuse`, and `rust` installed on your Linux environment. After that, building is as simple as using Rust's build tool:
`cargo build`

## Usage
`cargo run <args>` will execute the binary, which has built in usage help

To test the `mkfs` subcommand, use a file/block that is decently large (a few hundred MiBs will do). One can be created with the following command: `fallocate -l 1GiB testfile.dat`
## Docs
[TODO.md](doc/TODO.md)
[NOTES.md](doc/NOTES.md)