pub const FS_DEFAULT_BLOCK_SIZE: &str = "4096";
pub const FS_DEFAULT_INODES: &str = "4096";
pub const FS_MAGIC_VALUE: u32 = '🖬' as u32;
pub const FS_SUPERBLOCK_SIZE: u64 = 64;

pub const FS_DIR_ENTRY_SIZE: u64 = 256;
pub const FS_INODE_SIZE: u64 = 64;