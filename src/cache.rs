use crate::constants::*;
use crate::ltbfs::{DirEntry, Inode, SuperBlock};
use bitvec::prelude::*;
use byteorder::{LittleEndian, ReadBytesExt, WriteBytesExt};
use lru::LruCache;
use std::collections::HashMap;
use std::fs::File;
use std::io::{Error, ErrorKind, Read, Seek, SeekFrom, Write};

pub struct Cache {
    storage_device: File,
    block_size: u64,
    inodes: u64,
    blocks: u64,
    data: LruCache<u64, Vec<u8>>,
    inode_cache: LruCache<u64, Inode>,
    pub bitmap_data: BitVec<Msb0, u8>,
    pub bitmap_inodes: BitVec<Msb0, u8>,
    dirty_blocks: HashMap<u64, u64>,
}

impl Cache {
    /// Create/set a block to zeroes
    pub fn zero_block(&mut self, block_num: u64) {
        let block = vec![0u8; self.block_size as usize];
        self.dirty_blocks.insert(block_num, block_num);
        self.data.put(block_num, block);
    }

    /// Get readonly block by number
    pub fn get_block(&mut self, block_num: u64) -> Vec<u8> {
        match self.data.get(&block_num) {
            Some(value) => return value.clone(),
            None => {
                self.read_block(block_num);
                return self.get_block(block_num);
            }
        }
    }

    /// Returns a block (Vec<u8>) and sets block num as dirty (I assume you're going to mutate this block)
    fn get_block_mut(&mut self, block_num: u64) -> &mut Vec<u8> {
        self.get_block(block_num);
        let block: &mut Vec<u8>;
        let data = self.data.get_mut(&block_num);
        block = data.unwrap();
        self.dirty_blocks.insert(block_num, block_num);
        block
    }

    fn read_block(&mut self, block_num: u64) {
        self.zero_block(block_num);
        let block = self.data.get_mut(&block_num).unwrap();
        self.storage_device
            .seek(SeekFrom::Start(block_num * self.block_size))
            .unwrap();
        self.storage_device.read_exact(block).unwrap();
    }

    /// Get inode from disk or cache by number
    pub fn get_inode(&mut self, inode_number: u64) -> Inode {
        let inode_try = self.inode_cache.get(&inode_number);
        match inode_try {
            Some(value) => return *value,
            None => {
                self.read_inode(inode_number);
                return self.get_inode(inode_number);
            }
        }
    }

    pub fn set_superblock(
        &mut self,
        magic_header: u32,
        block_size: u32,
        num_blocks: u32,
        num_inodes: u32,
    ) {
        let block_num = 0;
        let block = self.get_block_mut(block_num);
        let mut writer = std::io::Cursor::new(block);
        writer.seek(SeekFrom::Start(0)).unwrap();
        writer.write_u32::<LittleEndian>(magic_header).unwrap();
        writer.write_u32::<LittleEndian>(block_size).unwrap();
        writer.write_u32::<LittleEndian>(num_blocks).unwrap();
        writer.write_u32::<LittleEndian>(num_inodes).unwrap();

        self.dirty_blocks.insert(block_num, block_num);
    }

    pub fn check_superblock(&mut self) {
        let block = self.get_block(0);
        let mut reader = std::io::Cursor::new(block);
        let magic_header = reader.read_u32::<LittleEndian>().unwrap();
        if magic_header != FS_MAGIC_VALUE {
            panic!("Bad superblock!");
        }
    }

    pub fn set_inode_from_struct(&mut self, inode: &Inode) {
        //self.set_inode(inode.is_valid, inode.inode_type, inode.perm, inode.inode_number, inode.size, inode.block_start, inode.atime, inode.mtime, inode.ctime, inode.crtime, inode.uid, inode.gid);
        self.set_inode(
            inode.is_valid,
            inode.inode_type,
            inode.rdev,
            inode.perm,
            inode.inode_number,
            inode.size,
            inode.block_start,
            inode.atime,
            inode.mtime,
            inode.ctime,
            inode.uid,
            inode.gid,
        );
        self.inode_cache
            .put(inode.inode_number as u64, inode.clone());
    }

    pub fn set_inode(
        &mut self,
        is_valid: u8,
        inode_type: u8,
        rdev: u32,
        perm: u32,
        inode_number: u32,
        size: u64,
        block_start: u64,
        atime: u64,
        mtime: u64,
        ctime: u64,
        uid: u32,
        gid: u32,
    ) {
        let block_num = self.get_inode_table_block_start()
            + ((inode_number as u64 * FS_INODE_SIZE) / self.block_size);
        let block_offset = (inode_number as u64 * FS_INODE_SIZE) % self.block_size;
        let block = self.get_block_mut(block_num as u64);
        let mut writer = std::io::Cursor::new(block);
        writer.seek(SeekFrom::Start(block_offset)).unwrap();
        writer.write_u8(is_valid).unwrap();
        writer.write_u8(inode_type).unwrap();
        writer.write_u32::<LittleEndian>(rdev).unwrap();
        // writer.write_u16::<LittleEndian>(perm).unwrap();
        writer.write_u32::<LittleEndian>(perm).unwrap();
        writer.write_u32::<LittleEndian>(inode_number).unwrap();
        writer.write_u64::<LittleEndian>(size).unwrap();
        writer.write_u64::<LittleEndian>(block_start).unwrap();
        writer.write_u64::<LittleEndian>(atime).unwrap();
        writer.write_u64::<LittleEndian>(mtime).unwrap();
        writer.write_u64::<LittleEndian>(ctime).unwrap();
        // writer.write_u64::<LittleEndian>(crtime).unwrap();
        writer.write_u32::<LittleEndian>(uid).unwrap();
        writer.write_u32::<LittleEndian>(gid).unwrap();

        self.dirty_blocks
            .insert(inode_number as u64, inode_number as u64);
        self.set_bitmap_inodes(vec![inode_number as u64], is_valid != 0); // This inode may or may not be used
        self.set_bitmap_data(vec![block_num as u64], is_valid != 0); // This block may or may not be used
                                                                     // TODO: range for data bitmap if inode was bigger than BLOCK_SIZE
    }

    /// Read an inode to the LRU cache from data cache/disk
    fn read_inode(&mut self, ino: u64) {
        let block_num =
            self.get_inode_table_block_start() + ((ino * FS_INODE_SIZE) / self.block_size);
        let block_offset = (ino * FS_INODE_SIZE) % self.block_size;
        let block = self.get_block(block_num);
        let mut reader = std::io::Cursor::new(block);
        reader.seek(std::io::SeekFrom::Start(block_offset)).unwrap();

        let is_valid = reader.read_u8().unwrap();
        let inode_type = reader.read_u8().unwrap();
        let rdev = reader.read_u32::<LittleEndian>().unwrap();
        let perm = reader.read_u32::<LittleEndian>().unwrap();
        let inode_number = reader.read_u32::<LittleEndian>().unwrap();
        let size = reader.read_u64::<LittleEndian>().unwrap();
        let block_start = reader.read_u64::<LittleEndian>().unwrap();
        let atime = reader.read_u64::<LittleEndian>().unwrap();
        let mtime = reader.read_u64::<LittleEndian>().unwrap();
        let ctime = reader.read_u64::<LittleEndian>().unwrap();
        // let crtime = reader.read_u64::<LittleEndian>().unwrap();
        let uid = reader.read_u32::<LittleEndian>().unwrap();
        let gid = reader.read_u32::<LittleEndian>().unwrap();

        let inode = Inode {
            is_valid: is_valid,
            inode_type: inode_type,
            rdev: rdev,
            perm: perm,
            inode_number: inode_number,
            size: size,
            block_start: block_start,
            atime: atime,
            mtime: mtime,
            ctime: ctime,
            // crtime: crtime,
            uid: uid,
            gid: gid,
        };
        self.inode_cache.put(ino as u64, inode);
    }

    pub fn get_inode_with_parent_and_name(&mut self, parent_num: u64, name: &str) -> Option<Inode> {
        let parent = self.get_inode(parent_num);
        let entries = self.get_dir_entries(&parent);
        for entry in entries {
            if *name.as_bytes() == entry.name[..name.len()] {
                return Some(self.get_inode(entry.inode as u64));
            }
        }
        None
    }

    fn get_free_dir_entry(&mut self, inode: &Inode) -> Option<(u64, u64)> {
        let mut block_num = inode.block_start;
        let block_end = block_num + inode.size / self.block_size;

        while block_num < block_end {
            let block = self.get_block(block_num);
            let mut reader = std::io::Cursor::new(&block);
            let mut block_offset = 0;
            while block_offset < self.block_size {
                reader.seek(SeekFrom::Start(block_offset)).unwrap();
                let ent_inode_num = reader.read_u32::<LittleEndian>().unwrap();
                if ent_inode_num == 0 {
                    // Actual entry, ignores empty entries - if a entry was removed in the middle
                    return Some((block_num, block_offset));
                }
                block_offset += FS_DIR_ENTRY_SIZE;
            }
            block_num += 1;
        }

        None
    }

    pub fn add_dir_entry(
        &mut self,
        inode: &Inode,
        entry_inode_num: u64,
        name: &str,
        name_len: u32,
    ) -> Result<(), Error> {
        match self.get_free_dir_entry(&inode) {
            Some(value) => {
                let (block_num, block_offset) = value;
                let block = self.get_block_mut(block_num); // Also sets dirty_block
                let mut writer = std::io::Cursor::new(block);
                writer.seek(SeekFrom::Start(block_offset)).unwrap();
                let mut name_vec = name.as_bytes().to_vec();
                writer
                    .write_u32::<LittleEndian>(entry_inode_num as u32)
                    .unwrap();
                writer.write_u32::<LittleEndian>(name_len).unwrap();
                writer.write(&mut name_vec).unwrap();
            }
            None => {
                return Err(Error::new(
                    ErrorKind::Other,
                    "Not enough space - no grow is implemented :(",
                ));
            }
        }
        Ok(())
    }

    /// Sets the dir entry at the location (block_num, block_offset)
    pub fn set_dir_entry_at_location(
        &mut self,
        location: (u64, u64),
        entry_inode_num: u64,
        name: &str,
    ) {
        let (block_num, block_offset) = location;
        let block = self.get_block_mut(block_num); // Also sets dirty_block
        let mut writer = std::io::Cursor::new(block);
        writer.seek(SeekFrom::Start(block_offset)).unwrap();
        let mut name_vec = name.as_bytes().to_vec();
        writer
            .write_u32::<LittleEndian>(entry_inode_num as u32)
            .unwrap();
        writer.write_u32::<LittleEndian>(name.len() as u32).unwrap();
        writer.write(&mut name_vec).unwrap();
    }

    /// Delete dir entry
    pub fn del_dir_entry_parent_and_name(&mut self, parent_num: u64, name: &str) {
        let parent = self.get_inode(parent_num);
        let mut block_num = parent.block_start;
        let block_end = block_num + parent.size / self.block_size;
        let mut del_block_num: u64;
        let mut del_block_offset: u64;

        {
            while block_num < block_end {
                let block = self.get_block(block_num);
                let mut reader = std::io::Cursor::new(&block);
                let mut block_offset = 0;
                while block_offset < self.block_size {
                    reader.seek(SeekFrom::Start(block_offset)).unwrap();
                    let ent_inode_num = reader.read_u32::<LittleEndian>().unwrap();
                    if ent_inode_num != 0 {
                        // Actual entry, ignores empty entries - if a entry was removed in the middle
                        let entry = self.get_dir_entry((block_num, block_offset));
                        if *name.as_bytes() == entry.name[..name.len()] {
                            // Remove entry by setting inode to 0
                            del_block_num = block_num;
                            del_block_offset = block_offset;

                            let block = self.get_block_mut(del_block_num);
                            let mut writer = std::io::Cursor::new(block);
                            writer.seek(SeekFrom::Start(del_block_offset)).unwrap();
                            writer.write_u32::<LittleEndian>(0 as u32).unwrap();
                        }
                    }
                    block_offset += FS_DIR_ENTRY_SIZE;
                }
                block_num += 1;
            }
        }
    }

    pub fn get_dir_entry_location_parent_and_name(
        &mut self,
        parent_num: u64,
        name: &str,
    ) -> Option<(u64, u64)> {
        let parent = self.get_inode(parent_num);
        let mut block_num = parent.block_start;
        let block_end = block_num + parent.size / self.block_size;
        {
            while block_num < block_end {
                let block = self.get_block(block_num);
                let mut reader = std::io::Cursor::new(&block);
                let mut block_offset = 0;
                while block_offset < self.block_size {
                    reader.seek(SeekFrom::Start(block_offset)).unwrap();
                    let ent_inode_num = reader.read_u32::<LittleEndian>().unwrap();
                    if ent_inode_num != 0 {
                        // Actual entry, ignores empty entries - if a entry was removed in the middle
                        let entry = self.get_dir_entry((block_num, block_offset));
                        if *name.as_bytes() == entry.name[..name.len()] {
                            // Return location of entry
                            return Some((block_num, block_offset));
                        }
                    }
                    block_offset += FS_DIR_ENTRY_SIZE;
                }
                block_num += 1;
            }
        }
        None
    }

    /// Return dir entry from block and offset
    pub fn get_dir_entry(&mut self, location: (u64, u64)) -> DirEntry {
        let (block_num, block_offset) = location;
        let block = self.get_block(block_num);
        let mut reader = std::io::Cursor::new(&block);
        reader.seek(SeekFrom::Start(block_offset)).unwrap();
        let inode = reader.read_u32::<LittleEndian>().unwrap();
        let name_len = reader.read_u32::<LittleEndian>().unwrap();
        let mut name_array = [0u8; 248];
        reader.read_exact(&mut name_array).unwrap();

        let dir_entry = DirEntry {
            inode,
            name_len,
            name: name_array,
        };

        dir_entry
    }

    /// ☢☢☢ Boom ☢☢☢ - Get Vec<DirEntry> from dir Inode
    pub fn get_dir_entries(&mut self, inode: &Inode) -> Vec<DirEntry> {
        let mut entries = Vec::<DirEntry>::new();
        let mut block_num = inode.block_start;
        let block_end = block_num + inode.size / self.block_size;

        while block_num < block_end {
            let block = self.get_block(block_num);
            let mut reader = std::io::Cursor::new(&block);
            let mut block_offset = 0;
            while block_offset < self.block_size {
                reader.seek(SeekFrom::Start(block_offset)).unwrap();
                let ent_inode_num = reader.read_u32::<LittleEndian>().unwrap();
                if ent_inode_num != 0 {
                    // Actual entry, ignores empty entries - if a entry was removed in the middle
                    entries.push(self.get_dir_entry((block_num, block_offset)));
                }
                block_offset += FS_DIR_ENTRY_SIZE;
            }
            block_num += 1;
        }

        entries
    }

    /// Return BitVec from bitmap from cache/disk using location (block_num, block_offset) and number of bits
    fn read_bitmap(&mut self, location: (u64, u64), bits: u64) -> BitVec<Msb0, u8> {
        let mut bitmap = vec![];
        let mut current_bit = 0;
        let (mut location_block, mut location_offset) = location;
        let mut location_offset_end: u64 = self.block_size;
        while current_bit < bits {
            // Load in next block
            let block = self.get_block(location_block);
            let mut bitmap_reader = std::io::Cursor::new(&block);
            // If last block then the end offset will change
            if bits - current_bit < self.block_size {
                location_offset_end = bits - current_bit;
            }
            // Seek start of data in block (usually 0)
            bitmap_reader
                .seek(std::io::SeekFrom::Start(location_offset))
                .unwrap();
            let mut data = vec![0u8; (location_offset_end - location_offset) as usize];
            bitmap_reader.read(&mut data).unwrap();
            bitmap.append(&mut data);
            current_bit += (location_offset_end - location_offset) * 8;
            location_block += 1;
            location_offset = 0;
            location_offset_end = self.block_size;
        }

        BitVec::<Msb0, u8>::from_vec(bitmap)
    }

    pub fn read_bitmap_inodes(&mut self) {
        let location = self.get_bitmap_inode_start();
        let bits = self.inodes;
        self.bitmap_inodes = self.read_bitmap(location, bits);
    }

    pub fn read_bitmap_data(&mut self) {
        let location = self.get_bitmap_data_start();
        let bits = self.blocks;
        self.bitmap_data = self.read_bitmap(location, bits);
    }

    pub fn get_free_data_blocks(&mut self, blocks: u64) -> Result<u32, Error> {
        let mut first_bit: u32 = 0;
        let mut num_contig = 0;
        for (index, bit) in (&self.bitmap_data).into_iter().enumerate() {
            if !bit {
                num_contig += 1;
                if num_contig == 1 {
                    first_bit = index as u32;
                }
                if num_contig == blocks {
                    return Ok(first_bit as u32);
                }
            } else {
                num_contig = 0;
            }
        }

        Err(Error::new(
            ErrorKind::Other,
            format!("Could not find {} free blocks", blocks),
        ))
    }

    pub fn get_free_inode(&mut self) -> Result<u64, Error> {
        for (index, bit) in (&self.bitmap_inodes).into_iter().enumerate() {
            if !bit {
                return Ok(index as u64);
            }
        }

        Err(Error::new(ErrorKind::Other, "Could not find free inode"))
    }

    pub fn set_bitmap_data(&mut self, indicies: Vec<u64>, value: bool) {
        let (location_block, location_block_offset) = self.get_bitmap_data_start();
        for index in indicies {
            let bitmap = &mut self.bitmap_data; //.get_mut(index as usize).unwrap() = value;
            *bitmap.get_mut(index as usize).unwrap() = value;
            let cur_offset = location_block_offset + (index / 8);
            let cur_block = location_block + cur_offset / self.block_size;
            let cur_offset = (location_block_offset + (index / 8)) % self.block_size;

            let mut byte: u8 = 0;
            let mut bit_index = 0;
            for bit in &bitmap[((index as usize / 8) * 8)..((index as usize / 8) * 8 + 8)] {
                byte = byte | (*bit as u8) << (7 - bit_index);
                bit_index += 1;
            }

            let block = self.get_block_mut(cur_block);
            *block.get_mut(cur_offset as usize).unwrap() = byte;
        }
    }

    pub fn set_bitmap_inodes(&mut self, indicies: Vec<u64>, value: bool) {
        let (location_block, location_block_offset) = self.get_bitmap_inode_start();
        for index in indicies {
            let bitmap = &mut self.bitmap_inodes;
            *bitmap.get_mut(index as usize).unwrap() = value;
            let cur_offset = location_block_offset + (index / 8);
            let cur_block = location_block + cur_offset / self.block_size;
            let cur_offset = (location_block_offset + (index / 8)) % self.block_size;

            let mut byte: u8 = 0;
            let mut bit_index = 0;
            for bit in &bitmap[((index as usize / 8) * 8)..((index as usize / 8) * 8 + 8)] {
                byte = byte | (*bit as u8) << (7 - bit_index);
                bit_index += 1;
            }

            let block: &mut Vec<u8> = self.get_block_mut(cur_block);
            *block.get_mut(cur_offset as usize).unwrap() = byte;
        }
    }

    /// Get block number for start of inode table
    fn get_inode_table_block_start(&self) -> u64 {
        let bitmap_sizes = (self.blocks + self.inodes) / 8;
        let start_size = bitmap_sizes + FS_SUPERBLOCK_SIZE; // Superblock is 64 bytes
        let blocks = ((start_size - 1) / self.block_size) + 1;
        blocks
    }

    /// Get block number for start of data
    pub fn get_data_block_start(&self) -> u64 {
        self.get_inode_table_block_start()
            + ((self.inodes * FS_INODE_SIZE + (self.block_size - 1)) / self.block_size)
    }

    /// Return block number and offset of inode bitmap (block_num, block_offset)
    fn get_bitmap_inode_start(&self) -> (u64, u64) {
        let location = FS_SUPERBLOCK_SIZE;
        let block_num = location / self.block_size;
        let block_offset = location % self.block_size;
        (block_num, block_offset)
    }

    /// Return block number and offset of data bitmap (block_num, block_offset)
    fn get_bitmap_data_start(&self) -> (u64, u64) {
        let location = self.get_bitmap_inode_start();
        let location = location.0 * self.block_size + location.1;
        let location = location + self.inodes / 8;
        let block_num = location / self.block_size;
        let block_offset = location % self.block_size;
        (block_num, block_offset)
    }

    pub fn write_data(&mut self, inode: &Inode, data: &[u8], offset: i64) -> Result<u64, Error> {
        let mut inode = inode.clone();
        if data.len() as usize > self.block_size as usize {
            println!("Uh oh, couldn't handle the buffer to write!");
            return Err(Error::new(
                ErrorKind::Other,
                "Write amount is greater than blocksize!",
            ));
        }
        // if (((inode.size as i64 - 1)/ self.block_size as i64) as u64) < (offset as u64 + data.len() as u64) / self.block_size {
        if inode.size == 0 {
            let block_start = self.get_free_data_blocks(64).unwrap() as u64;
            let mut used_blocks = Vec::<u64>::new();
            for index in block_start..(block_start + 64) {
                used_blocks.push(index);
            }
            self.set_bitmap_data(used_blocks, true);
            inode.block_start = block_start;
            self.set_inode_from_struct(&inode);
        } else {
            // match self.grow_contiguous(&inode, 1) {
            //     Err(e) => return Err(e),
            //     Ok(_)  => {},
            // }
        }
        // }

        if inode.block_start == 0 {
            println!("Superblock was about to be overwritten! :(");
            return Err(Error::new(
                ErrorKind::Other,
                "About to write over superblock!",
            ));
        }
        let mut block_num = inode.block_start;
        block_num += offset as u64 / self.block_size;
        let block_offset = offset as u64 % self.block_size;
        let block = self.get_block_mut(block_num);
        let mut writer = std::io::Cursor::new(block);
        writer.seek(SeekFrom::Start(block_offset)).unwrap();
        writer.write_all(data).unwrap();
        if data.len() as u64 + offset as u64 > inode.size {
            inode.size = data.len() as u64 + offset as u64;
        }
        self.set_inode_from_struct(&inode);
        Ok(data.len() as u64)
    }

    pub fn read_data(&mut self, inode: &Inode, offset: i64, size: u64) -> Result<Vec<u8>, Error> {
        if inode.size == 0 {
            return Ok(vec![]);
            //return Err(Error::new(ErrorKind::Other, "Size is 0"));
        }

        let mut block_num = inode.block_start;
        block_num += offset as u64 / self.block_size;
        let mut block_offset = offset as u64 % self.block_size;
        let mut buffer_out = vec![0u8; size as usize];
        {
            let mut writer = std::io::Cursor::new(&mut buffer_out);
            let mut written = 0;
            let mut to_read: usize;
            while written < size as usize {
                if (size - written as u64) < (self.block_size - block_offset) {
                    to_read = size as usize - written;
                } else {
                    to_read = self.block_size as usize;
                }
                let block = self.get_block(block_num);
                let mut reader = std::io::Cursor::new(block);
                reader.seek(SeekFrom::Start(block_offset)).unwrap();
                let data = &mut vec![0u8; to_read];
                reader.read_exact(data).unwrap();
                written += writer.write(data).unwrap();
                block_offset = 0;
                block_num += 1;
            }
        }

        return Ok(buffer_out);
    }

    //
    // Unfortunately never worked quite right - would like to fix some time
    pub fn grow_contiguous(&mut self, inode: &Inode, size: u64) -> Result<(), Error> {
        let next_block = inode.block_start + (inode.size - 1) / self.block_size + 1;
        let mut new_blocks = Vec::<u64>::new();
        for index in next_block..(next_block + size) {
            new_blocks.push(index);
        }
        for index in &new_blocks {
            if self.bitmap_data[*index as usize] == true {
                println!(
                    "DataBitmapAroundGrow{:?}",
                    &self.bitmap_data[*index as usize..(*index + 16) as usize]
                );
                return Err(Error::new(
                    ErrorKind::Other,
                    "Can not grow to requested size.",
                ));
            }
        }
        self.set_bitmap_data(new_blocks, true);
        Ok(())
    }

    /// Write all dirty_blocks to disk and then clear HashMap
    pub fn flush_cache_to_disk(&mut self) {
        let mut dirty_blocks: Vec<u64> = self.dirty_blocks.iter().map(|(key, _)| *key).collect();
        dirty_blocks.sort_by(|x, y| x.cmp(y));
        for block_num in dirty_blocks {
            let block = self.get_block(block_num);
            self.storage_device
                .seek(SeekFrom::Start(block_num * self.block_size))
                .unwrap();
            self.storage_device.write(&block).unwrap();
        }
        self.dirty_blocks.clear();
    }

    pub fn new(file: File, superblock: SuperBlock) -> Cache {
        Cache {
            storage_device: file,
            block_size: superblock.block_size as u64,
            inodes: superblock.num_inodes as u64,
            blocks: superblock.num_blocks as u64,
            data: LruCache::new(1024),
            inode_cache: LruCache::new(1024),
            bitmap_inodes: BitVec::new(),
            bitmap_data: BitVec::new(),
            dirty_blocks: HashMap::new(),
        }
    }
}
