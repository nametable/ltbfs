use crate::cache::Cache;
use crate::constants::*;
use byteorder::{LittleEndian, ReadBytesExt};
/**
 * Logan Bateman
 * 2020-04-12
 * Main implementation
 **/
use fuser::{
    FileAttr, FileType, Filesystem, KernelConfig, MountOption, ReplyAttr, ReplyData,
    ReplyDirectory, ReplyEmpty, ReplyEntry, ReplyOpen, ReplyStatfs, ReplyWrite, Request,
};
use libc::{c_int, EEXIST, ENOENT, ENOSPC, ENOSYS, ENOTEMPTY};
use std::ffi::OsStr;
use std::fs::{File, OpenOptions};
use std::io::Seek;
use std::time::{Duration, SystemTime, UNIX_EPOCH};

const TTL: Duration = Duration::from_secs(1); // 1 second

pub enum InodeType {
    Directory = 0,
    File = 1,
}

#[derive(Copy, Clone)]
pub struct SuperBlock {
    pub magic_header: u32,
    pub block_size: u32,
    pub num_blocks: u32,
    pub num_inodes: u32,
}

// 63 bytes (Will be stored as 64)
#[derive(Copy, Clone)]
pub struct Inode {
    pub is_valid: u8,
    pub inode_type: u8,
    pub rdev: u32,
    pub perm: u32,
    pub inode_number: u32,
    pub size: u64,
    pub block_start: u64,
    pub atime: u64,
    pub mtime: u64,
    pub ctime: u64,
    // pub crtime: u64,
    pub uid: u32,
    pub gid: u32,
}

pub fn attr_from_inode(inode: &Inode, superblock: &SuperBlock) -> FileAttr {
    let kind: FileType;
    if inode.inode_type == InodeType::Directory as u8 {
        kind = FileType::Directory;
    } else if inode.inode_type == InodeType::File as u8 {
        kind = FileType::RegularFile;
    } else {
        kind = FileType::BlockDevice
    };

    FileAttr {
        ino: inode.inode_number as u64,
        size: inode.size,
        blocks: (inode.size + superblock.block_size as u64 - 1) / superblock.block_size as u64,
        atime: UNIX_EPOCH + Duration::from_secs(inode.atime), // 1970-01-01 00:00:00
        mtime: UNIX_EPOCH + Duration::from_secs(inode.mtime),
        ctime: UNIX_EPOCH + Duration::from_secs(inode.ctime),
        crtime: UNIX_EPOCH + Duration::from_secs(inode.ctime),
        kind: kind,
        perm: inode.perm as u16,
        nlink: 1, // Hard links - every file and dir must have one - I only support 1 currently
        uid: inode.uid,
        gid: inode.gid,
        rdev: 0, // Only for devices
        flags: 0,
        blksize: 0, // ??? MacOS only
    }
}

impl Default for Inode {
    fn default() -> Self {
        Inode {
            is_valid: 0,
            inode_type: InodeType::Directory as u8,
            rdev: 0,
            perm: 0o777,
            inode_number: 0,
            size: 0,
            block_start: 0,
            atime: 0,
            mtime: 0,
            ctime: 0,
            // crtime: 0,
            uid: 0,
            gid: 0,
        }
    }
}

// 256 Bytes
#[derive(Copy, Clone)]
pub struct DirEntry {
    pub inode: u32,
    pub name_len: u32,
    pub name: [u8; 248], // Fixed length file/dir name of 248
}

// https://stackoverflow.com/questions/28127165/how-to-convert-struct-to-u8
// pub unsafe fn any_as_u8_slice<T: Sized>(p: &T) -> &[u8] {
//     ::std::slice::from_raw_parts(
//         (p as *const T) as *const u8,
//         ::std::mem::size_of::<T>(),
//     )
// }

// pub fn write_superblock(file: &mut File, magic_header: u32, block_size: u32, num_blocks: u32, num_inodes: u32) {

//     let mut superblock_array = vec![];

//     superblock_array.write_u32::<LittleEndian>(magic_header).unwrap();
//     superblock_array.write_u32::<LittleEndian>(block_size).unwrap();
//     superblock_array.write_u32::<LittleEndian>(num_blocks).unwrap();
//     superblock_array.write_u32::<LittleEndian>(num_inodes).unwrap();

//     file.seek(std::io::SeekFrom::Start(0)).unwrap();
//     file.write_all(&superblock_array).unwrap();

// }

/// Read superblock from disk
pub fn read_superblock(file: &mut File) -> SuperBlock {
    file.seek(std::io::SeekFrom::Start(0)).unwrap();

    let magic_header = file.read_u32::<LittleEndian>().unwrap();
    let block_size = file.read_u32::<LittleEndian>().unwrap();
    let num_blocks = file.read_u32::<LittleEndian>().unwrap();
    let num_inodes = file.read_u32::<LittleEndian>().unwrap();

    if magic_header != FS_MAGIC_VALUE {
        panic!("Bad superblock!");
    }

    SuperBlock {
        magic_header: magic_header,
        block_size: block_size,
        num_blocks: num_blocks,
        num_inodes: num_inodes,
    }
}

pub struct LTBFS {
    cache: Cache,
    superblock: SuperBlock,
}

impl Filesystem for LTBFS {
    fn init(&mut self, _req: &Request<'_>, kernel_config: &mut KernelConfig) -> Result<(), c_int> {
        self.cache.read_bitmap_data();
        self.cache.read_bitmap_inodes();
        println!(
            "First 32 of inode bitmap: {:?}",
            &self.cache.bitmap_inodes[..32]
        );
        println!(
            "First 32 of data bitmap : {:?}",
            &self.cache.bitmap_data[..32]
        );
        Ok(())
    }

    // Look up a directory entry by name and get its attributes.
    fn lookup(&mut self, _req: &Request, _parent: u64, name: &OsStr, reply: ReplyEntry) {
        let parent = self.cache.get_inode(_parent);
        let entries = self.cache.get_dir_entries(&parent);
        for entry in entries {
            if *name.to_str().unwrap().as_bytes() == entry.name[..name.len()] {
                let inode = self.cache.get_inode(entry.inode as u64);
                let attributes = attr_from_inode(&inode, &self.superblock);
                reply.entry(&TTL, &attributes, 0);
                return;
            }
        }

        reply.error(ENOENT);
    }

    // Get file attributes.
    fn getattr(&mut self, _req: &Request, ino: u64, reply: ReplyAttr) {
        let superblock = self.superblock;
        let inode = self.cache.get_inode(ino as u64);
        let file_attr = attr_from_inode(&inode, &superblock);

        reply.attr(&TTL, &file_attr);
    }

    fn setattr(
        &mut self,
        _req: &Request<'_>,
        ino: u64,
        mode: Option<u32>,
        uid: Option<u32>,
        gid: Option<u32>,
        size: Option<u64>,
        atime: Option<fuser::TimeOrNow>,
        mtime: Option<fuser::TimeOrNow>,
        _ctime: Option<SystemTime>,
        _fh: Option<u64>,
        _crtime: Option<SystemTime>,
        _chgtime: Option<SystemTime>,
        _bkuptime: Option<SystemTime>,
        _flags: Option<u32>,
        reply: ReplyAttr,
    ) {
        let mut inode = self.cache.get_inode(ino);

        match mode {
            Some(mode) => inode.perm = mode as u32,
            None => {}
        };
        match uid {
            Some(uid) => inode.uid = uid,
            None => {}
        };
        match gid {
            Some(gid) => inode.gid = gid,
            None => {}
        };
        match size {
            Some(size) => { /* inode.size = size */ } // todo: figure out why setattr would change size
            None => {}
        };
        match atime {
            Some(atime) => match atime {
                fuser::TimeOrNow::SpecificTime(atime) => {
                    inode.atime = atime.duration_since(UNIX_EPOCH).unwrap().as_secs()
                }
                fuser::TimeOrNow::Now => unimplemented!("atime = now"),
            },
            None => {}
        };
        match mtime {
            Some(mtime) => match mtime {
                fuser::TimeOrNow::SpecificTime(mtime) => {
                    inode.mtime = mtime.duration_since(UNIX_EPOCH).unwrap().as_secs()
                }
                fuser::TimeOrNow::Now => unimplemented!("mtime = now"),
            },
            None => {}
        };
        // match crtime {
        //     Some(crtime)   => inode.crtime = crtime.duration_since(UNIX_EPOCH).unwrap().as_secs(),
        //     None        => {},
        // };

        self.cache.set_inode_from_struct(&inode);
        let file_attr = attr_from_inode(&inode, &self.superblock);
        self.cache.flush_cache_to_disk();
        reply.attr(&TTL, &file_attr)
    }

    fn readdir(
        &mut self,
        _req: &Request,
        ino: u64,
        _fh: u64,
        offset: i64,
        mut reply: ReplyDirectory,
    ) {
        let inode = self.cache.get_inode(ino);
        let entries = self.cache.get_dir_entries(&inode);

        for (i, entry) in entries.into_iter().enumerate().skip(offset as usize) {
            // i + 1 means the index of the next entry
            let inode = self.cache.get_inode(entry.inode as u64);
            let kind = if inode.inode_type == InodeType::Directory as u8 {
                FileType::Directory
            } else if inode.inode_type == InodeType::File as u8 {
                FileType::RegularFile
            } else {
                FileType::BlockDevice
            };

            reply.add(
                entry.inode as u64,
                (i + 1) as i64,
                kind,
                std::str::from_utf8(&entry.name[..entry.name_len as usize]).unwrap(),
            );
        }

        reply.ok();
    }

    fn mkdir(
        &mut self,
        req: &Request<'_>,
        parent: u64,
        name: &OsStr,
        mode: u32,
        umask: u32,
        reply: ReplyEntry,
    ) {
        let parent = self.cache.get_inode(parent);
        let parent_children = self.cache.get_dir_entries(&parent);
        for child in parent_children {
            if *name.to_str().unwrap().as_bytes() == child.name[..name.len()] {
                reply.error(EEXIST); // Entry name already exists
                return;
            }
        }

        let inode_number = self.cache.get_free_inode().unwrap();
        let now = SystemTime::now()
            .duration_since(SystemTime::UNIX_EPOCH)
            .expect("Couldn't get time")
            .as_secs() as u64;
        let new_dir_data_block_start = self.cache.get_free_data_blocks(1024).unwrap();

        let inode = Inode {
            is_valid: 1,
            inode_type: InodeType::Directory as u8,
            rdev: 0,
            perm: mode,
            inode_number: inode_number as u32,
            size: 4096,
            block_start: new_dir_data_block_start as u64,
            atime: now,
            mtime: now,
            ctime: now,
            // crtime: now,
            uid: req.uid(),
            gid: req.gid(),
        };

        let dir_entry_location = self.cache.get_free_data_blocks(1).unwrap();
        let vec_used_inodes = vec![inode_number];
        let vec_used_data = vec![dir_entry_location as u64];
        // Write dir entry for new dir to parent
        match self.cache.add_dir_entry(
            &parent,
            inode.inode_number as u64,
            name.to_str().unwrap(),
            name.len() as u32,
        ) {
            Err(_) => {
                reply.error(ENOSPC); // No space
                return;
            }
            Ok(_) => {}
        }

        // Write inode
        self.cache.set_inode_from_struct(&inode);

        // Write . and .. entries for new directory
        self.cache.zero_block(inode.block_start); // Clear old data in block
        self.cache
            .add_dir_entry(&inode, inode.inode_number as u64, ".", 1 as u32)
            .unwrap();
        self.cache
            .add_dir_entry(&inode, parent.inode_number as u64, "..", 2 as u32)
            .unwrap();
        self.cache.set_bitmap_inodes(vec_used_inodes, true); // Mark newly used inodes
        self.cache.set_bitmap_data(vec_used_data, true); // Mark newly used data block
        println!(
            "Directory {} created with inode# {} and block_start {}",
            name.to_str().unwrap(),
            inode.inode_number,
            inode.block_start
        );
        println!(
            "First 64 of inode bitmap: {:?}",
            &self.cache.bitmap_inodes[..64]
        );
        reply.entry(&TTL, &attr_from_inode(&inode, &self.superblock), 0);
        self.cache.flush_cache_to_disk();
    }

    /// Remove a directory.
    fn rmdir(&mut self, _req: &Request<'_>, _parent: u64, _name: &OsStr, reply: ReplyEmpty) {
        match self
            .cache
            .get_inode_with_parent_and_name(_parent, _name.to_str().unwrap())
        {
            Some(mut value) => {
                let entries = self.cache.get_dir_entries(&value);
                if entries.len() < 3 {
                    self.cache
                        .set_bitmap_inodes(vec![value.inode_number as u64], false);
                    self.cache
                        .set_bitmap_data(vec![value.block_start as u64], false);
                    value.is_valid = 0;
                    self.cache.set_inode_from_struct(&value);
                    self.cache
                        .del_dir_entry_parent_and_name(_parent, _name.to_str().unwrap());
                    self.cache.flush_cache_to_disk();
                    println!(
                        "Directory {} deleted with inode# {} and block_start {}",
                        _name.to_str().unwrap(),
                        value.inode_number,
                        value.block_start
                    );
                    println!(
                        "First 64 of inode bitmap: {:?}",
                        &self.cache.bitmap_inodes[..64]
                    );
                    reply.ok();
                    return;
                } else {
                    reply.error(ENOTEMPTY);
                    return;
                }
            }
            None => {
                reply.error(ENOENT);
                return;
            }
        }
    }

    fn mknod(
        &mut self,
        req: &Request<'_>,
        _parent: u64,
        name: &OsStr,
        mode: u32,
        umask: u32,
        _rdev: u32,
        reply: ReplyEntry,
    ) {
        let entry = self
            .cache
            .get_dir_entry_location_parent_and_name(_parent, name.to_str().unwrap());
        if entry.is_some() {
            reply.error(EEXIST);
            return;
        }
        let now = SystemTime::now()
            .duration_since(SystemTime::UNIX_EPOCH)
            .expect("Couldn't get time")
            .as_secs() as u64;
        let inode_number = self.cache.get_free_inode().unwrap();

        let inode = Inode {
            is_valid: 1,
            inode_type: InodeType::File as u8,
            rdev: 0,
            perm: mode,
            inode_number: inode_number as u32,
            size: 0,
            block_start: 0 as u64,
            atime: now,
            mtime: now,
            ctime: now,
            // crtime: now,
            uid: req.uid(),
            gid: req.gid(),
        };

        let parent = self.cache.get_inode(_parent);
        // Write dir entry for new dir to parent
        match self.cache.add_dir_entry(
            &parent,
            inode.inode_number as u64,
            name.to_str().unwrap(),
            name.len() as u32,
        ) {
            Err(_) => {
                reply.error(ENOSPC); // No space
                return;
            }
            Ok(_) => {}
        }

        self.cache.set_inode_from_struct(&inode);
        self.cache.flush_cache_to_disk();
        reply.entry(&TTL, &attr_from_inode(&inode, &self.superblock), 0);
        println!(
            "File '{}' with inode# {} has been created.",
            name.to_str().unwrap(),
            inode_number
        );
        println!("InodeBitmap(64): {:?}", &self.cache.bitmap_inodes[..64]);
        //reply.error(ENOSYS);
    }

    /// Rename a file.
    fn rename(
        &mut self,
        _req: &Request<'_>,
        _parent: u64,
        _name: &OsStr,
        _newparent: u64,
        _newname: &OsStr,
        flags: u32,
        reply: ReplyEmpty,
    ) {
        let src_dir_entry_loc = self
            .cache
            .get_dir_entry_location_parent_and_name(_parent, _name.to_str().unwrap())
            .unwrap();
        let src_dir_entry = self.cache.get_dir_entry(src_dir_entry_loc);

        if _parent == _newparent {
            // Easy, just change the name
            self.cache.set_dir_entry_at_location(
                src_dir_entry_loc,
                src_dir_entry.inode as u64,
                _newname.to_str().unwrap(),
            );
            self.cache.flush_cache_to_disk();
            reply.ok();
            println!(
                "Renamed '{}' with inode# {} to '{}'",
                _name.to_str().unwrap(),
                src_dir_entry.inode,
                _newname.to_str().unwrap()
            );
            return;
        }
        // TODO: make sure newname doesn't already exist

        reply.error(ENOSYS);
    }

    /// Remove a file.
    fn unlink(&mut self, _req: &Request<'_>, parent: u64, name: &OsStr, reply: ReplyEmpty) {
        let mut inode = self
            .cache
            .get_inode_with_parent_and_name(parent, name.to_str().unwrap())
            .unwrap();
        inode.is_valid = 0;
        self.cache.set_inode_from_struct(&inode); // Invalidate inode
        self.cache
            .del_dir_entry_parent_and_name(parent, name.to_str().unwrap()); // remove entry
        let mut free_blocks = Vec::<u64>::new();
        let next_block = (inode.block_start as i64
            + (inode.size as i64 - 1) / self.superblock.block_size as i64)
            as u64
            + 1;
        for index in inode.block_start..next_block {
            free_blocks.push(index);
        }
        if inode.block_start != 0 {
            self.cache.set_bitmap_data(free_blocks, false); // Free unused data blocks in bitmap
        }
        self.cache.flush_cache_to_disk();
        println!(
            "File '{}' with inode# {} has been unlinked (deleted)",
            name.to_str().unwrap(),
            inode.inode_number
        );
        reply.ok();

        // reply.error(ENOSYS);
    }

    /// Open a file.
    /// Open flags (with the exception of O_CREAT, O_EXCL, O_NOCTTY and O_TRUNC) are
    /// available in flags. Filesystem may store an arbitrary file handle (pointer, index,
    /// etc) in fh, and use this in other all other file operations (read, write, flush,
    /// release, fsync). Filesystem may also implement stateless file I/O and not store
    /// anything in fh. There are also some flags (direct_io, keep_cache) which the
    /// filesystem may set, to change the way the file is opened. See fuse_file_info
    /// structure in <fuse_common.h> for more details.
    fn open(&mut self, _req: &Request<'_>, _ino: u64, _flags: i32, reply: ReplyOpen) {
        reply.opened(0, 0);
    }

    /// Read data.
    /// Read should send exactly the number of bytes requested except on EOF or error,
    /// otherwise the rest of the data will be substituted with zeroes. An exception to
    /// this is when the file has been opened in 'direct_io' mode, in which case the
    /// return value of the read system call will reflect the return value of this
    /// operation. fh will contain the value set by the open method, or will be undefined
    /// if the open method didn't set any value.
    fn read(
        &mut self,
        _req: &Request<'_>,
        _ino: u64,
        _fh: u64,
        offset: i64,
        size: u32,
        flags: i32,
        lock_owner: Option<u64>,
        reply: ReplyData,
    ) {
        let inode = self.cache.get_inode(_ino);
        let data = self.cache.read_data(&inode, offset, size as u64).unwrap();
        reply.data(&data);
        println!(
            "Reading {} bytes at offset {} from file with inode# {}",
            size, offset, _ino
        );
        //reply.data(&"testing123\n".as_bytes()[offset as usize..]);
        //reply.error(ENOSYS);
    }

    /// Write data.
    /// Write should return exactly the number of bytes requested except on error. An
    /// exception to this is when the file has been opened in 'direct_io' mode, in
    /// which case the return value of the write system call will reflect the return
    /// value of this operation. fh will contain the value set by the open method, or
    /// will be undefined if the open method didn't set any value.
    fn write(
        &mut self,
        _req: &Request<'_>,
        _ino: u64,
        _fh: u64,
        offset: i64,
        data: &[u8],
        _write_flags: u32,
        _flags: i32,
        _lock_owner: Option<u64>,
        reply: ReplyWrite,
    ) {
        let inode = self.cache.get_inode(_ino);
        let written;
        match self.cache.write_data(&inode, data, offset) {
            Err(_e) => {
                reply.error(ENOSPC);
                return;
            }
            Ok(w) => written = w,
        }
        self.cache.flush_cache_to_disk();
        reply.written(written as u32);
        println!("File with inode# {} has been written to", _ino);
        println!("{} bytes: {:?}", written, data);
        // reply.error(ENOSYS);
    }

    /// Get file system statistics.
    fn statfs(&mut self, _req: &Request<'_>, _ino: u64, reply: ReplyStatfs) {
        reply.statfs(0, 0, 0, 0, 0, 512, 255, 0);
    }
}

impl LTBFS {}

pub fn mount(mount_path: std::path::PathBuf, sd_path: std::path::PathBuf) {
    let options = vec![MountOption::RW, MountOption::FSName("ltbfs".to_string())];

    let mut file = OpenOptions::new()
        .write(true)
        .read(true)
        .open(sd_path)
        .expect("Couldn't open the file in mount()");
    let superblock = read_superblock(&mut file);

    fuser::mount2(
        LTBFS {
            cache: Cache::new(file, superblock),
            superblock: superblock,
        },
        mount_path,
        &options,
    )
    .expect("Could not mount!");

    // fuser::spawn_mount2(
    //     LTBFS {
    //         cache: Cache::new(file, superblock),
    //         superblock: superblock,
    //     },
    //     mount_path,
    //     &options,
    // )
    // .expect("Could not mount!")
}
