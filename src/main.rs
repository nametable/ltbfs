use std::io::stdin;
/**
 * Logan Bateman
 * 2020-04-12
 * LTBFS entrypoint
 *
 * Some inspiration from http://siciarz.net/24-days-of-rust-fuse-filesystems-part-1/
 *
 **/
use structopt::StructOpt;
mod cache;
mod constants;
mod ltbfs;
mod mkfs;
// mod tui;     Not using

#[link(name = "ltbfshelper", kind = "static")]
extern "C" {
    // this is rustified prototype of the functions from C
    // fn testcall(v: f32);
    // fn array_test(buffer: *mut u8, length: u32);
}

#[derive(StructOpt, Debug)]
#[structopt(
    name = "ltbfs",
    about = "LTBFS is a fuse based filesystem written by Logan Bateman."
)]
struct Cli {
    #[structopt(subcommand)]
    cmd: Command,
}

#[derive(StructOpt, Debug)]
enum Command {
    Mkfs {
        #[structopt(parse(from_os_str))]
        device_path: std::path::PathBuf,
        #[structopt(short = "b", long = "block-size", default_value = constants::FS_DEFAULT_BLOCK_SIZE)]
        block_size: u32,
        #[structopt(short = "i", long = "inodes", default_value = constants::FS_DEFAULT_INODES)]
        inodes: u32,
    },
    Mount {
        #[structopt(parse(from_os_str))]
        mount_path: std::path::PathBuf,
        #[structopt(parse(from_os_str))]
        device_path: std::path::PathBuf,
    },
}

fn main() {
    // println!("Hello, world from Rust!");
    // let my_file = File::open("testfile.dat").expect("Couldn't open file");
    // let mut reader = BufReader::new(my_file);
    // let mut test: [u8; 5] = unsafe {std::mem::uninitialized()};
    // reader.read_exact(&mut test).expect("Couldn't read it");
    // println!("Byte is {:?}", test);

    let args: Cli = Cli::from_args();
    let cmd = args.cmd;

    match cmd {
        Command::Mkfs {
            device_path,
            block_size,
            inodes,
        } => {
            mkfs::mkfs(device_path, block_size, inodes);
        }
        Command::Mount {
            mount_path,
            device_path: mount_device_path,
        } => {
            ltbfs::mount(mount_path, mount_device_path);

            let mut user_str = String::new();
            stdin().read_line(&mut user_str).unwrap();
        }
    }
}
