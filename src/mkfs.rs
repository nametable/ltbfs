use std::fs::{OpenOptions};
use std::time::{SystemTime};
use crate::constants::*;
use crate::ltbfs::*;
use crate::cache::{Cache};

#[allow(unused)] // TODO - actually remove unused variables
pub fn mkfs(fs_path: std::path::PathBuf, fs_block_size: u32, fs_inodes: u32) {

    let fs_path_metadata = fs_path.to_owned();
    let metadata = std::fs::metadata(fs_path_metadata).expect("Coundn't get file metadata");
    let mut file = OpenOptions::new().write(true).read(true).open(fs_path).expect("Couldn't open the file in mkfs()");
    //let mut writer = BufWriter.new(file);
    let file_size = metadata.len();

    let fs_num_blocks = (file_size / fs_block_size as u64) as u32;
    
    let superblock = SuperBlock {
        magic_header: FS_MAGIC_VALUE,
        block_size: fs_block_size,
        num_blocks: fs_num_blocks,
        num_inodes: fs_inodes,
    };
    
    
    // write_superblock(&mut file, FS_MAGIC_VALUE, fs_block_size, fs_num_blocks, fs_inodes);
    // read_superblock(&mut file);
    
    let mut cache = Cache::new(file, superblock);
    
    cache.read_bitmap_data();
    cache.read_bitmap_inodes();
    let mut data_blocks_to_zero = Vec::<u64>::new();
    for index in 0..fs_num_blocks as u64-1 {
        data_blocks_to_zero.push(index);
    }
    let mut inodes_to_zero = Vec::<u64>::new();
    for index in 0..fs_inodes as u64-1 {
        inodes_to_zero.push(index);
    }
    let mut data_blocks_to_one = Vec::<u64>::new();
    for index in 0..cache.get_data_block_start() {
        data_blocks_to_one.push(index);
    }
    cache.set_superblock(FS_MAGIC_VALUE, fs_block_size, fs_num_blocks, fs_inodes);
    cache.set_bitmap_data(data_blocks_to_zero, false);
    cache.set_bitmap_data(data_blocks_to_one, true);
    cache.set_bitmap_inodes(inodes_to_zero, false);
    cache.flush_cache_to_disk();

    let now = SystemTime::now().duration_since(SystemTime::UNIX_EPOCH).expect("Couldn't get time").as_secs() as u64;

    cache.set_bitmap_inodes(vec![0], true); // Inode 0 is not free
    cache.set_bitmap_data(vec![0], true); // Block 0 is not free
    let data_start_block = cache.get_free_data_blocks(1).unwrap();
    cache.set_inode(1, InodeType::Directory as u8, 0, 0o755, 1, superblock.block_size as u64, data_start_block as u64, now, now, now, 1000, 1000);
    cache.set_bitmap_data(vec![data_start_block as u64], true);

    let root = cache.get_inode(1);
    cache.zero_block(root.block_start);
    cache.add_dir_entry(&root, 1, ".", 1);  // Current dir root
    cache.add_dir_entry(&root, 1, "..", 2);  // Current dir root
    cache.add_dir_entry(&root, 0, "testfile.txt", 12);  // Current dir root
    cache.flush_cache_to_disk();

    cache.check_superblock();
    println!("Finished writing fs to disk!");
    println!("First 32 of inode bitmap: {:?}", &cache.bitmap_inodes[..64]);
    println!("First 32 of data bitmap : {:?}", &cache.bitmap_data[..64]);
    cache.read_bitmap_inodes();
    cache.read_bitmap_data();
    println!("First 32 of inode bitmap: {:?}", &cache.bitmap_inodes[..64]);
    println!("First 32 of data bitmap : {:?}", &cache.bitmap_data[..64]);
}