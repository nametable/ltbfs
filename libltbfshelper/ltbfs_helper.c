#include <stdio.h>

void testcall(float value)
{
    printf("Hello, world from C! Value passed: %f\n",value);
}

void array_test(char *buffer, unsigned int len) {
    for (unsigned int i = 0; i < len; i += 4) {
        (*(int *)(buffer+i)) = i;
    }
}