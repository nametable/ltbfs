[README.md](/README.md)
# Notes
 - Do some wierd stuff to make C and Rust play together :)
https://flames-of-code.netlify.app/blog/rust-and-cmake/

 - Part of the journey - asking why my Rust is slower than my C
https://stackoverflow.com/questions/61261256/why-is-my-rust-slower-than-my-c-memory-manipulation


DISK

|Offset  | Size  | Description
|---     | ---   | ---
|0x0     |1MiB   |PARTITIONTABLE
|0x1Mi   |xGiB   |PARTITION 1
|0x?     |xGiB   |PARTITION 2

LTBFS FS ON PARTITION 1

| Offset | Size | Description|
|-------|------|------------|
|       |       |**SUPER BLOCK**   (1 Block)
|       |4B    |`MAGIC HEADER    (char)`
|       |4B    |`BLOCK SIZE      (u32) min:512B max:512MiB (2^29)`
|       |4B    |`NUM OF BLOCKS   (u32) min:2    max:2^128 (disk space)`
|       |4B    |`NUM OF INODES   (u32) min:256  max:2^128 (disk space)`
|       |       |**Inode**
|       |1B    |`is_valid           (u8)`
|       |1B    |`inode_type         (u8)`
|       |4B    |`rdev               (u32)`
|       |4B    |`perm               (u32)`
|       |4B    |`inode_number       (u32)`
|       |8B    |`size               (u64)`
|       |8B    |`block_start        (u64)`
|       |8B    |`atime              (u64)`
|       |8B    |`mtime              (u64)`
|       |8B    |`ctime              (u64)`
|       |4B    |`uid                (u32)`
|       |4B    |`gid                (u32)`
|       |       |**DirEntry**
|       |4B    |`inode              (u32)`
|       |4B    |`name_len           (u32)`
|       |248B  |`name               ([u8; 248])`
