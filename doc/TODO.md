[README.md](/README.md)
# TODO List
 - [X] (regular) Ability to bootstrap an fs - utlity like mkfs (v2)
    - [x] Write superblock
    - [x] Ability to write directory entry at arbitrary locations
 - [X] (regular) Ability to write files
 - [X] (regular) Ability to read files
 - [/] (regular) Ability to move inodes (files, folders) in directory hierarchy
 - [X] (regular) Ability to delete inodes/files
 - [ ] (regular) Ability to grow file until contiguous space is unavailable
 - [X] (regular) read and write inode attributes (read, write, exec for user, group, other)
    - [X] Read inode
    - [X] Write inode
 - [X] (regular) Create, delete folders
 - [X] (regular) extent-based (pointer, length as opposed to fixed sized blocks)
 - [ ] (extra) file can grow beyond the space available (requires moving to an area with more free space)
 - [ ] (extra) symlinks
 - [ ] (extra) kernel module
 - [ ] (extra) easter egg files/folders that do funny things if you know how to use them (hidden files/folders)
 - [ ] (extra) internal data compression... not from scratch but using some library